<?php

use Devisr\Template\Template;
use PHPUnit\Framework\TestCase;

class TemplateTest extends TestCase {

    public function testVarTag() {
        $tpl = new Template("@var{test}");
        $tpl->test = "a";

        $this->assertEquals("a", (string)$tpl);
    }

    public function testIncludeTag() {
        $tpl = new Template("@include{tests/includes/test.php}");
        $this->assertEquals("test", (string)$tpl);
    }

    public function testArrayTag() {
        $tpl = new Template("@array{names,Name\, @name\n}");
        $tpl->push("names", [ "name" => "Bob"]);
        $tpl->push("names", [ "name" => "Harry"]);

        $this->assertEquals("Name, Bob\nName, Harry\n", (string)$tpl);
    }
    
}