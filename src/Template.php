<?php

namespace Devisr\Template;

use \Closure;

/**
 * Engine for simple, logic-less PHP templates
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class Template {
    const TAG_REGEX = "/\@([a-z]+){(.*?)}/is";

    protected $tpl;
    protected $vars;
    protected $arrays = [];
    public $recursion = true;
    public $recursionLimit = 3;

    static protected $tags = [];
    static protected $hooks = [];

    /**
     * Constructs a new Template object
     *
     * @param string $tpl the template to use, can either be a string or file
     */
    public function __construct(string $tpl) {
        if(is_file($tpl) && !file_exists($tpl)) {
            throw new InvalidArgumentException("Parameter \$tpl was specified as file: {$tpl} but the file does not exist");
        }

        $this->tpl = is_file($tpl) ? file_get_contents($tpl) : $tpl;
    }

    /**
     * Calls dynamically set up hooks
     *
     * @param string $name the name of the hook to call
     * @param array $args an array of arguments to pass to the hook
     * @return mixed the return value of the hook that was called
     */
    public function __call(string $name, array $args) {
        if(isset(self::$hooks[$name])) {
            return self::$hooks[$name]->call($this, ...$args);
        }
    }

    /**
     * Sets vars for the var tag
     *
     * @param string $name the name of the variable
     * @param mixed $value the name to set for the variable
     */
    public function __set(string $name, $value) {
        $this->vars[$name] = $value;
    }

    /**
     * Gets vars previously set up for the var tag
     * 
     * @param string $name the name of the var to get
     * @param mixed the value of the var or null if it does not exist.
     */
    public function __get(string $name) {
        return $this->vars[$name] ?? null;
    }

    /**
     * Parses and returns the resulting string
     *
     * @return string the parsed template
     */
    public function __toString() {
        return $this->parse($this->tpl);
    }

    /**
     * Parses the template
     *
     * @param string $tpl the template string
     * @return string the parsed template
     */
    protected function parse(string $tpl): string {
        static $tries = 1;

        $tpl = preg_replace_callback(self::TAG_REGEX, [$this, "parseCallback"], $this->tpl);

        if($this->recursion && $tries <= $this->recursionLimit && preg_match(self::TAG_REGEX, $tpl)) {
            $tries++;
            return $this->process($tpl);
        } return $tpl;
    }

    /**
     * Helper method for Template::parse, used inside the preg_replace_callback function.
     * This performs the lookup for individual tag replacements.
     *
     * @param array $matches an array of matches that the regex function returned
     * @return string the resulting string to replace the tag with
     */
    protected function parseCallback($matches): string {
        $tag = $matches[1];
        $parameters = $matches[2];

        return (isset(self::$tags[$tag])) ? self::$tags[$tag]->call($this, ...(preg_split("/(?<=[^".preg_quote('\\')."]),/", $parameters))) : "";
    }

    /**
     * Adds a tag handler
     *
     * @param string $name the name of the tag
     * @param Closure $callback the tag handler
     * @return void
     */
    static public function addTag(string $name, Closure $callback) {
        self::$tags[$name] = $callback;
    }

    /**
     * Adds a hook (dynamic template method for manipulating tags in php)
     *
     * @param string $name the name of the hook
     * @param Closure $callback the handler for the hook
     * @return void
     */
    static public function addHook(string $name, Closure $callback) {
        self::$hooks[$name] = $callback;
    }
}

include "Defaults.php"; // setup defaults
