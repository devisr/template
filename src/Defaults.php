<?php

namespace Devisr\Template;

/**
 * Variables -------------
 * (@var{variablename})
 * Set with hook: $tpl->variablename = "..."
 */
Template::addTag("var", function($var) {
    return $this->vars[$var] ?? "";
});

/**
 * Includes -------------
 * (@include{file.php})
 */
Template::addTag("include", function($file) {
    ob_start();
    if(file_exists($file)) include $file;
    return trim(ob_get_clean());
});

/**
 * Arrays -------------
 * (@array{name,Something to be repeated with @variables})
 * Set with hook: $tpl->push("name", [ "@variables" => "replacement" ]);
 */
Template::addTag("array", function($key, $tpl) {
    ob_start();
    if(array_key_exists($key, $this->arrays) && is_array($this->arrays[$key])) {
        foreach($this->arrays[$key] as $array) {
            if(!is_array($array)) continue;

            $tplcopy = $tpl;
            foreach($array as $k => $v) $tplcopy = str_replace("@{$k}", $v, $tplcopy);
            echo $tplcopy;
        }
    }
    return stripslashes(ob_get_clean());
});

/**
 * Hook for the array tag
 */
Template::addHook("push", function($array, $value) {
    if(!array_key_exists($array, $this->arrays)) $this->arrays[$array] = [];
    $this->arrays[$array][] = $value;
});

